from flask import Flask, request
import os

cur_path = os.path.dirname(os.path.realpath(__file__))
parent_path = os.path.abspath(os.path.join(cur_path, os.pardir))
compile_cmd = parent_path + '/Arduino/arduino_debug --verify ' + parent_path + '/testArduino/testArduino.ino'
upload_cmd = parent_path + '/Arduino/arduino_debug --upload ' + parent_path + '/testArduino/testArduino.ino'

app = Flask(__name__)


def write_ino_file(sketch):
    test_path = parent_path + '/testArduino/testArduino.ino'
    with open(test_path, 'w') as f:
        f.write(sketch)


@app.route('/')
def hello():
    return 'BlocklyDuino Server started...'


@app.route('/compile', methods=['POST'])
def compile_arduino():
    run_arduino('compile', request.data)
    return 'Compile success'


@app.route('/upload', methods=['POST'])
def upload_arduino():
    run_arduino('upload', request.data)
    return 'Upload success'


def run_arduino(action, sketch):
    try:
        write_ino_file(sketch)
        if action == 'compile':
            os.system(compile_cmd)
        elif action == 'upload':
            os.system(upload_cmd)
    except Exception as e:
        print e


if __name__ == '__main__':
    app.run()
